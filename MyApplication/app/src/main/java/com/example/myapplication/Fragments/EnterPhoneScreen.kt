package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import kotlinx.android.synthetic.main.enter_phone_screen.view.*

class EnterPhoneScreen: Fragment() {

    lateinit var mListener: OnEnterPhoneScreenListener

    interface OnEnterPhoneScreenListener {
        fun changeActBar(buttonState: Boolean, title: String)
        fun changeFragment(fragment: Fragment)
    }

    override fun onStart() {
        mListener.changeActBar(true, "Ваш телефон")
        super.onStart()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.enter_phone_screen,container,false)

        v.continueButt.setOnClickListener {
            val enterCodeScreen = EnterCodeScreen()
            mListener.changeFragment(enterCodeScreen)
        }

        return v
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnEnterPhoneScreenListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragment1DataListener")
        }
    }

    override fun onPause() {
        mListener.changeActBar(false, "Beauty House")
        super.onPause()
    }
}