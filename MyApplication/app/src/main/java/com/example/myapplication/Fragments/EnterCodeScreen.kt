package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import kotlinx.android.synthetic.main.enter_code_screen.view.*

class EnterCodeScreen: Fragment(){

    lateinit var mListener: OnEnterCodeScreenListener


    interface OnEnterCodeScreenListener {
        fun changeActBar(buttonState: Boolean, title: String)
        fun getHomeScreen()
    }

    override fun onStart() {
        mListener.changeActBar(true, "Подтверждение")
        super.onStart()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.enter_code_screen,container,false)

        v.continueButt.setOnClickListener {
            mListener.getHomeScreen()
        }

        return v
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnEnterCodeScreenListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragment1DataListener")
        }
    }

    override fun onStop() {
        mListener.changeActBar(false,"Beauty House")
        super.onStop()
    }
}