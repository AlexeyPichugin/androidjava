package com.example.myapplication.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import kotlinx.android.synthetic.main.auth_screen.*

class AuthScreen: Fragment() {

    interface OnAuthScreenListener {
        fun changeFragment(fragment: Fragment)
    }

    lateinit var mListener: OnAuthScreenListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.auth_screen,container,false)

        val logInBut: Button = v.findViewById(R.id.logInButt)
        val regBut = regButt

        logInBut.setOnClickListener {
            val enterPhoneScreen = EnterPhoneScreen()
            mListener.changeFragment(enterPhoneScreen)
        }

        return v
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnAuthScreenListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragment1DataListener")
        }
    }
}