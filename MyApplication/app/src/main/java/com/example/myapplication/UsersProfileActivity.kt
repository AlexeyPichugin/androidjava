package com.example.myapplication

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.user_profile_activity.*
import android.provider.MediaStore
import android.content.Intent
import android.R.attr.data
import android.app.Activity
import androidx.core.app.NotificationCompat.getExtras
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import java.io.File


class UsersProfileActivity : AppCompatActivity() {

    private val CAMERA_REQUEST = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_profile_activity)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        userProfileCameraFloatButton.setOnClickListener {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.edit_profile -> {
            val intent = Intent(this,EditProfileActivity::class.java)
            startActivity(intent)
            true
        }
        R.id.logout_profile -> {
            Toast.makeText(this, "LOGOUT", Toast.LENGTH_LONG).show()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === CAMERA_REQUEST && resultCode === Activity.RESULT_OK) {
            // Фотка сделана, извлекаем картинку
            val thumbnailBitmap = data?.getExtras()?.get("data") as Bitmap
            profileImage.setImageBitmap(thumbnailBitmap)
        }
    }

}