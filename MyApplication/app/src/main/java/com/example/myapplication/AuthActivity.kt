package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.myapplication.Fragments.AuthScreen
import com.example.myapplication.Fragments.EnterCodeScreen
import com.example.myapplication.Fragments.EnterPhoneScreen


class AuthActivity : AppCompatActivity(), AuthScreen.OnAuthScreenListener,EnterPhoneScreen.OnEnterPhoneScreenListener,EnterCodeScreen.OnEnterCodeScreenListener {

    val fragmentManager: FragmentManager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_activity)

        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        val authScreen = AuthScreen()
        ft.add(R.id.containerMainActivity,authScreen,"authScreen")
        ft.commit()
    }

    override fun changeFragment(fragment: Fragment){
        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        ft.replace(R.id.containerMainActivity,fragment,"logInScreen")
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun changeActBar(buttonState: Boolean, title: String) {
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(buttonState)
            actionBar.setDisplayHomeAsUpEnabled(buttonState)
            actionBar.title = title
        }
    }

    override fun getHomeScreen(){
        val intent = Intent(this,MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}